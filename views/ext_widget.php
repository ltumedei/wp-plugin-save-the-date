<!-- This file is used to markup the public-facing widget. -->

<?php
	/**
	 * Get the global post to be able to set the postdata
	 */
	global $post;
	foreach ($events as $event) {
		$post = $event;
		setup_postdata( $post );

        $event_category = '';
        $terms = get_the_terms($post_id, 'event-category');
        if( $terms && !is_wp_error($terms) ){
            if (count($terms) >= 1 ) {
                // pick the first event category coming across
                $event_category = reset($terms)->name;
            }
        }

        $province = get_post_meta($event->ID, $key = '_province', $single = true);

        $cat_color = '';
        if (function_exists('the_main_category') ) {
         $main_category = Main_Category_Picker::get_main_category( $post->ID );
         if (function_exists('get_the_category_color')) {
             $cat_color = get_the_category_color( $main_category->term_id );
         }
     }
     else{
         $cat_color = '#222222';
     }
     $background_color_style = "style=\"background-color:$cat_color\"";

     if (function_exists('eo_get_the_start')) {
         $date = eo_get_the_start('d.m', $event->ID, null, $event->occurrence_id);
         if ($date == '') {
            $date = '-';
        }
    }
    else{
     $date = '-';
 }
 ?>
 <div class="event">
     <a href="<?php echo post_permalink($post->ID); ?>">
        <div class="left_side" <?php echo $background_color_style; ?>>
           <ul>
              <li class="title vert_center_text">
                 <!-- the title of the event -->
                 <?php the_title(); ?>
             </li>
             <li class="subtitle vert_center_text">
                 <!-- the subtitle of the event -->
                 <?php if (function_exists('the_subtitle')): ?>
                 <?php the_subtitle(); ?>
             <?php endif ?>
         </li>
         <li class="author vert_center_text">
          <!-- the author of the event -->
          <a href="<?php get_author_posts_url( get_the_author_meta( $field = 'ID') ); ?>">
             di <?php the_author(); ?>
         </a>
     </li>
     <li class="content">
      <!-- the content of the event -->
      (...) <span class="black-text">LEGGI TUTTO</span>
  </li>
</ul>
</div>
<div class="right_side">
    <ul>
       <li class="province vert_center_text">
          <!-- the province  in the format (MI) -->
          <?php echo "($province)"; ?>
      </li>
      <hr>
      <li class="date vert_center_text">
          <!-- the date in the formt "dd.mm" -->
          <?php echo $date; ?>
      </li>
      <hr>
      <li class="category vert_center_text fit-text">
          <!-- the main subcategory for the event -->
          <?php
          if ( function_exists('the_main_category'))
             echo $event_category;
         ?>
     </li>
 </ul>
</div>
</a>
</div>
<?php
} //end of foreach
wp_reset_query();
?>