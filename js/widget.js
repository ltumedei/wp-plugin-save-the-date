(function( $ ) {
	"use strict";

	$(function() {
	// TODO get the right side width dynamically to avoid hardwiring in bind_hover_event function
	set_events_measures();
	animate_event_labels();
	vert_align_text();
});
}(jQuery));
function set_events_measures(){
	jQuery('.sidebar .event').each( set_event_measures );
} // end of set_events_measures
function set_event_measures(){
	var magnifier = 1.2; // used to fine tune title and content height
	var ev = jQuery(this);
	var left_side = ev.find('.left_side');
	var title = left_side.find('.title');
	var title_h = title.height() * magnifier;
	var content = left_side.find('.content');
	var content_h = content.height() * magnifier;
	var event_h = title_h + content_h;
	title.css('height' , title_h + 'px' );
	content.css('height' , content_h + 'px' );
	if (event_h > 84) {
		var event_h_str = event_h + 'px'; // height and width retunr px values only!
		ev.css('height' , event_h_str);
		left_side.css('height' , '100%'); //needed to make the bg image fit
} // end of if
else {
	ev.css('height' , '84px');
	left_side.css('height' , '100%'); //needed to make the bg image fit
	} // end of else
} // end of set_event_height
function animate_event_labels(){
	hide_left_sides();
	var events = jQuery('.sidebar .event');
	events.each( bind_hover_event );
} // end of animate_event_labels
function hide_left_sides(){
	jQuery('.sidebar .event .left_side').css('width' , 0);
} // end of hide_left_sides
function bind_hover_event(){
	var ev = jQuery(this);
	ev.hover(
		// TODO use right side width here and not an hardcoded number
		function(){
			ev.find('.left_side').filter(':not(:animated)').animate( { width : '65%' }, 200,
				function(){
					ev.find('.left_side li').show(100);
					setTimeout(
						function (){
							ev.find('.left_side').filter(':not(:animated)').animate( { width : 0 }, 200 );
						}, 3000 ); // end of setTimeout
				}
				);
		},
		function(){
			ev.find( '.left_side li').hide(100 ,
				function(){
					ev.find('.left_side').filter(':not(:animated)').animate( { width : 0 }, 200 );
				}
				);
		}
		);
} // end of bind_hover_event
function vert_align_text(){
	var img = get_vert_image();
	jQuery('.vert_align_text').prepend( img );
} // end of vert_align_text
function get_vert_image(){
	return '<img src="../images/t.png"style="width:1px; height:100%; vertical-align:middle;">';
} // end of get_vert_image