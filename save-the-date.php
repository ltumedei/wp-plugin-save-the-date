<?php
/*
Plugin Name: Save The Date
Plugin URI: http://www.makemydaymag.com
Description: A widget displaying the incoming and current events (req. Events Organiser Plugin)
Version: 1.0
Author: theAverageDev (Luca Tumedei)
Author URI: http://theaveragedev.com
Author Email: luca@theaveragedev.com
Text Domain: save_the_date
Domain Path: /lang/
Network: false
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Copyright 2012 TODO (email@domain.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

class Save_The_Date extends WP_Widget {

	/*--------------------------------------------------*/
	/* Constructor
	/*--------------------------------------------------*/

	/**
	 * Specifies the classname and description, instantiates the widget,
	 * loads localization files, and includes necessary stylesheets and JavaScript.
	 */
	public function __construct() {

		// load plugin text domain
		add_action( 'init', array( $this, 'widget_textdomain' ) );

		// Hooks fired when the Widget is activated and deactivated
		register_activation_hook( __FILE__, array( $this, 'activate' ) );
		register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );

		parent::__construct(
			'save_the_date',
			__( 'Save The Date', 'save_the_date' ),
			array(
				'classname'		=>	'Save_The_Date',
				'description'	=>	__( 'Displays SaveTheDate.', 'save_the_date' )
				)
			);

		// Register admin styles and scripts
		add_action( 'admin_print_styles', array( $this, 'register_admin_styles' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'register_admin_scripts' ) );

		// Register site styles and scripts
		add_action( 'wp_enqueue_scripts', array( $this, 'register_widget_styles' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'register_widget_scripts' ) );

	} // end constructor

	/*--------------------------------------------------*/
	/* Widget API Functions
	/*--------------------------------------------------*/

	/**
	 * Outputs the content of the widget.
	 *
	 * @param	array	args		The array of form elements
	 * @param	array	instance	The current instance of the widget
	 */
	public function widget( $args, $instance ) {

		extract( $args, EXTR_SKIP );

		echo $before_widget;

		$args = array(
			//all the posts
			'numberposts' => $instance['max_number_of_events_to_display'],
           //right from the first
			'offset' => 0,
           //older ones go first
			'orderby' => 'eventstart',
           //published events only
			'post_status' => 'publish'
			);
/**
 * if the required function from the Events Organiser plugin from Tom McFarlin exists
 * then fetch the set number of events
 */
		if(function_exists('eo_get_events')) {
			$events = eo_get_events($args);
		}

		/**
		 * if there are events then display the widget
		 */
		if( isset($events) and count($events) > 0 ) {
			include( plugin_dir_path( __FILE__ ) . '/views/widget.php' );
		}

		echo $after_widget;

	} // end widget

	/**
	 * Processes the widget's options to be saved.
	 *
	 * @param	array	new_instance	The previous instance of values before the update.
	 * @param	array	old_instance	The new instance of values to be generated via the update.
	 */
	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;

		// TODO:	Here is where you update your widget's old values with the new, incoming values
		$instance['max_number_of_events_to_display'] = strip_tags( $new_instance ['max_number_of_events_to_display'] );

		return $instance;

	} // end widget

	/**
	 * Generates the administration form for the widget.
	 *
	 * @param	array	instance	The array of keys and values for the widget.
	 */
	public function form( $instance ) {
		$defaults = array( 'max_number_of_events_to_display' => 6 );
		$instance = wp_parse_args( (array) $instance, $defaults );
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'max_number_of_events_to_display' ); ?>">
				<?php _e( 'Max number of events to display?', $domain = 'save_the_date' ); ?>
			</label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'max_number_of_events_to_display' ); ?>" name="<?php echo $this->get_field_name( 'max_number_of_events_to_display' ); ?>" type="text" value="<?php echo $instance['max_number_of_events_to_display']; ?>" />
		</p>
		<?php

		// Display the admin form
		include( plugin_dir_path(__FILE__) . '/views/admin.php' );

	} // end form

	/*--------------------------------------------------*/
	/* Public Functions
	/*--------------------------------------------------*/

	/**
	 * Loads the Widget's text domain for localization and translation.
	 */
	public function widget_textdomain() {

		load_plugin_textdomain( 'save_the_date', false, plugin_dir_path( __FILE__ ) . '/lang/' );

	} // end widget_textdomain

	/**
	 * Fired when the plugin is activated.
	 *
	 * @param		boolean	$network_wide	True if WPMU superadmin uses "Network Activate" action, false if WPMU is disabled or plugin is activated on an individual blog.
	 */
	public function activate( $network_wide ) {
		// TODO define activation functionality here
	} // end activate

	/**
	 * Fired when the plugin is deactivated.
	 *
	 * @param	boolean	$network_wide	True if WPMU superadmin uses "Network Activate" action, false if WPMU is disabled or plugin is activated on an individual blog
	 */
	public function deactivate( $network_wide ) {
		// TODO define deactivation functionality here
	} // end deactivate

	/**
	 * Registers and enqueues admin-specific styles.
	 */
	public function register_admin_styles() {

		wp_enqueue_style( 'save-the-date-admin-styles', plugins_url( 'save-the-date/css/admin.css' ) );

	} // end register_admin_styles

	/**
	 * Registers and enqueues admin-specific JavaScript.
	 */
	public function register_admin_scripts() {

		wp_enqueue_script( 'save-the-date-admin-script', plugins_url( 'save-the-date/js/admin.js' ) );

	} // end register_admin_scripts

	/**
	 * Registers and enqueues widget-specific styles.
	 */
	public function register_widget_styles() {

		wp_enqueue_style( 'save-the-date-widget-styles', plugins_url( 'save-the-date/css/widget.css' ) );

	} // end register_widget_styles

	/**
	 * Registers and enqueues widget-specific scripts.
	 */
	public function register_widget_scripts() {
		// the script depends on jQuery and it comes bundled into Wordpress
		wp_enqueue_script( 'save-the-date-script', plugins_url( 'save-the-date/js/widget.js' ), array('jquery') );

	} // end register_widget_scripts

} // end class

// TODO:	Remember to change 'Save_The_Date' to match the class name definition
add_action( 'widgets_init', create_function( '', 'register_widget("Save_The_Date");' ) );